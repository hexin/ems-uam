﻿function Money(BigInteger, value) {

	this.BigInteger = BigInteger;
	this.NUMBER_LENGTH = 20;
	this.NUMBER_OF_BANK_NOTES = 100;
	this.NUMBER_OF_INNER_ITEMS = 100;
	this.value = new BigInteger('' + (value * 10));

	// losowanie dowolnego ciągu (ze znaków 0-9) o stałej dlugości
	this.generateRandomBigInteger = function() {
		var result = '';
		for (var i=0 ; i<this.NUMBER_LENGTH ; i++) {
			result += Math.floor(Math.random() * 10);
		}
		return new BigInteger(result);
	}
	
	// konwersja dowolnego hex na BigInt (zamiana sha256 do ciągu cyfr 0-9)
	this.hexToBigInt = function(hexStr) {
		//console.log('===========================================================================');
		//console.log('Begin args: ' + hexStr.toString());
		
		// constants
		var base = new BigInteger('65536');
		var length = hexStr.length;
		var steps = Math.floor(length / 4);
		var rest = length % 4;
		//console.log('' + length + ' = ' + steps + ' * 4 + ' + rest);
		
		// variables
		var result = new BigInteger('0');
		var remaining = hexStr;
		var n = new BigInteger('1');
		
		for (var i=steps ; i > 0 ; i--) {
			var word = remaining.substring(length - 4);
			remaining = remaining.substring(0, length - 4);
			length -= 4;
			//console.log(word);
			result = result.add(new BigInteger(parseInt(word, 16).toString()).multiply(n));
			n = n.multiply(base);
		}
		//console.log(remaining);
		if (remaining.length > 0) {
			result = result.add(new BigInteger(parseInt(remaining, 16).toString()).multiply(n));
			n = n.multiply(base);
		}
		
		return result;
	}
	
	this.stringToBigInt = function(str) {
		var result = new BigInteger('0');
		var offset = new BigInteger('256');
		var exponent = new BigInteger('1');
		for (var i=str.length-1 ; i >= 0 ; i--) {
			var charVal = str.charCodeAt(i).toString();
			var newVal = new BigInteger(charVal).multiply(exponent);
			//console.log(charVal + ' * ' + exponent.toString() + ' = ' + newVal.toString());
			//console.log(newVal.toString() + ' + ' + result.toString());
			result = result.add(newVal);
			//console.log(' = ' + result.toString());
			exponent = exponent.multiply(offset);
		}
		return result;
	}

	/*
	{
		Y - string,		// ustalona kwota
		X - string,		// identyfikator banknotu
		S[] - string		// losowy ciąg kontrolny
		U[] - string		// hash Sha256 dla S
		T[] - string		// losowy ciąg kontrolny
		W[] - string		// hash Sha256 dla T
	}
	*/
	var M = []; // Lista 100 banknotów (struktura banknotu jest powyżej)
	
	// tworzenie pieniądza
	
	var I = []; // Lista 100 losowych ciągów BigInteger identyfikujących Alice
	for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		I[i] = [];
		for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
			I[i][j] = this.generateRandomBigInteger();
		}
	}
	
	var R = []; // Lista 100 losowych ciągów BigInteger (tyle samo bitów co I)
	for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		R[i] = [];
		for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
			R[i][j] = this.generateRandomBigInteger();
		}
	}
	
	var L = []; // Obliczamy XOR R i I
	for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		L[i] = [];
		for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
			L[i][j] = I[i][j].xor(R[i][j]);
		}
	}
	
	// sprawdzenie
	var checkResult = true;
	for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
			checkResult = checkResult && I[i][j].equals(L[i][j].xor(R[i][j]));
		}
	}
	console.log('I, R i L wylosowano poprawnie: ' + checkResult);
	
	var B = []; // Tablica 100x100 losowych ciągów (potrzbne do wyliczenia U i S)
	var C = []; // Tablica 100x100 losowych ciągów (potrzbne do wyliczenia W i T)
	
	// generowanie pieniędzy
	for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
	
		B[i] = [];
		C[i] = [];
		M[i] = {
			Y: this.value,
			X: this.generateRandomBigInteger(),
			S: [],
			T: [],
			U: [],
			W: []
		};
		
		var currentM = M[i];
		
		for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
		
			currentM.S[j] = this.generateRandomBigInteger();
			currentM.T[j] = this.generateRandomBigInteger();
			
			B[i][j] = this.generateRandomBigInteger();
			currentM.U[j] = this.hexToBigInt(Sha256.hash(currentM.S[j].toString() + B[i][j].toString() + L[i][j].toString()));
			
			C[i][j] = this.generateRandomBigInteger();
			currentM.W[j] = this.hexToBigInt(Sha256.hash(currentM.T[j].toString() + C[i][j].toString() + R[i][j].toString()));
		}
	}
	
	console.log('Pieniądze zostały wygenerowanie!');
	
	this.getBankNote = function(i) {
		return M[i];
	}
	
	this.getAllBankNotes = function() {
		return M;
	}
	
	//var coveredM = [];
	var hashedM = [];
	var omitted = null;
	var Z = [];
	
	this.setj = function(j) {
		this.omitted = j;
	}
	
	this.coverAll = function(K) {
		
		var E = new BigInteger(K.E);
		var N = new BigInteger(K.N);
		
		for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
			//console.log('Covering ' + i);
		
			Z[i] = this.generateZ(N);
		
			var currentM = M[i];
			var Zmod = Z[i].modPow(E, N);
			
			hashedM[i] = Sha256.hash(JSON.stringify(currentM));
			
			var tmp = this.hexToBigInt(hashedM[i]);
			//console.log('START');
			//var tmp = this.stringToBigInt(JSON.stringify(currentM));
			//console.log('END');
			
			//console.log('hashedM[i] ' + hashedM[i]);
			//console.log('Długość banknotu ' + tmp.toString().length);
			//console.log('Długość Zmod ' + Zmod.toString().length);
			//console.log('Długość E ' + E.toString().length);
			
			//coveredM[i] = this.multiplyMod(tmp, Zmod, E);
		}
		
		console.log('Banknoty zostały zakryte!');
	}
	
	this.getCoveredBankObject = function() {
	
		console.log('STATYSTYKI');
		var strM = JSON.stringify(M[0]);
		//console.log('Pojedynczy banknot: ' + strM);
		console.log('Długość pojedynczego banknotu: ' + strM.length);
	
		console.log('Przygotowuję paczkę dla banku...');
		/*
		var result = [];
		
		for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		
			var tmpS = [];
			var tmpB = [];
			var tmpL = [];
			for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
				tmpS[j] = M[i].S[j].toString();
				tmpB[j] = B[i][j].toString();
				tmpL[j] = L[i][j].toString();
			}
		
			result[i] = {
				Z: Z[i].toString(),
				Y: coveredM[i].toString(),
				S: tmpS,
				B: tmpB,
				L: tmpL,
				hashedM: hashedM[i]
			}
		}
		
		console.log('Paczka dla banku gotowa!');
		return result;
		*/
		
		var result = [];
		
		for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
			result[i] = hashedM[i];
		}
		
		console.log('Paczka dla banku gotowa!');
		return result;
	}
	
	this.getVerificationData = function() {
		console.log('Przygotowuję elemnty do weryfikacji...');
	
		var result = [];
		
		for (var i=0 ; i<this.NUMBER_OF_BANK_NOTES ; i++) {
		
			if (i == omitted) {
				result[i] = {
					Y: null,
					X: null,
					S: [],
					U: [],
					T: [],
					W: [],
					L: [],
					R: [],
					B: [],
					C: [],
					I: [],
					Z: null
				}
			}
			else {
				result[i] = {
					Y: M[i].Y.toString(),
					X: M[i].X.toString(),
					S: [],
					U: [],
					T: [],
					W: [],
					L: [],
					R: [],
					B: [],
					C: [],
					I: [],
					Z: Z[i].toString()
				}
				
				for (var j=0 ; j<this.NUMBER_OF_INNER_ITEMS ; j++) {
					result[i].S[j] = M[i].S[j].toString();
					result[i].U[j] = M[i].U[j];
					result[i].T[j] = M[i].T[j].toString();
					result[i].W[j] = M[i].W[j];
					result[i].L[j] = L[i][j].toString();
					result[i].R[j] = R[i][j].toString();
					result[i].B[j] = B[i][j].toString();
					result[i].C[j] = C[i][j].toString();
					result[i].I[j] = I[i][j].toString();
				}
			}
		}
		
		console.log('Paczka weryfikująca dla banku gotowa!');
		return result;
	}
	
	var banknote = null;
	
	this.setBanknote = function(arg) {
		this.banknote = arg;
	}
	
	this.multiplyMod = function(a, Zmod, E) {
		return a.multiply(Zmod).mod(E);
	}
	
	this.generateZ = function(N) {
	
		var Z;
		var ans;
		
		//console.log('Genereating Z for N=' + K.N);
		
		do {
			Z = this.generateRandomBigInteger();
			//console.log('Z=' + Z.toString());
			ans = this.Euklides(Z, N).toString();
		} while (ans != '1');
		
		return Z;
	}
	
	this.Euklides = function(a, b) {
		// https://en.wikipedia.org/wiki/Euclidean_algorithm
		// do not use copare for 0!!!
		// library has defect
		var t;
		while (b.toString() != '0') {
			t = b.clone();
			b = a.divideAndRemainder(b)[1];
			a = t;
		}
		return a;
	}
	
	this.test = function() {
		
		var result;
		
		result = this.hexToBigInt('ffff');
		console.log('test 1 (hexToBigInt) >> ' + result.toString() + ' == 65535 >> ' + (result.toString() == '65535'));
		
		result = this.hexToBigInt('0');
		console.log('test 2 (hexToBigInt) >> ' + result.toString() + ' == 0 >> ' + (result.toString() == '0'));
		
		result = this.hexToBigInt('abcd');
		console.log('test 3 (hexToBigInt) >> ' + result.toString() + ' == 43981 >> ' + (result.toString() == '43981'));
		
		result = this.hexToBigInt('7fffffffffffffff');
		console.log('test 4 (hexToBigInt) >> ' + result.toString() + ' == 9223372036854775807 >> ' + (result.toString() == '9223372036854775807'));
		
		result = this.hexToBigInt('1234567890abcdef');
		console.log('test 5 (hexToBigInt) >> ' + result.toString() + ' == 1311768467294899695 >> ' + (result.toString() == '1311768467294899695'));
		
		result = this.hexToBigInt('10000');
		console.log('test 6 (hexToBigInt) >> ' + result.toString() + ' == 65536 >> ' + (result.toString() == '65536'));
		
		result = this.hexToBigInt('100000000');
		console.log('test 7 (hexToBigInt) >> ' + result.toString() + ' == 4294967296 >> ' + (result.toString() == '4294967296'));
		
		result = this.Euklides(new BigInteger('2322'), new BigInteger('654'));
		console.log('test 8 (Euklides) >> ' + result.toString() + ' == 6 >> ' + (result.toString() == '6'));
		
		result = this.Euklides(new BigInteger('654'), new BigInteger('2322'));
		console.log('test 9 (Euklides) >> ' + result.toString() + ' == 6 >> ' + (result.toString() == '6'));
		
		result = this.Euklides(new BigInteger('243'), new BigInteger('111'));
		console.log('test 10 (Euklides) >> ' + result.toString() + ' == 3 >> ' + (result.toString() == '3'));
		
		result = this.Euklides(new BigInteger('111'), new BigInteger('243'));
		console.log('test 11 (Euklides) >> ' + result.toString() + ' == 3 >> ' + (result.toString() == '3'));
		
		result = this.stringToBigInt('A');
		console.log('test 12 (stringToBigInt) >> ' + result.toString() + ' == 65 >> ' + (result.toString() == '65'));
		
		result = this.stringToBigInt('AB');
		console.log('test 13 (stringToBigInt) >> ' + result.toString() + ' == 16706 >> ' + (result.toString() == '16706'));
		
		result = this.stringToBigInt('ABC');
		console.log('test 14 (stringToBigInt) >> ' + result.toString() + ' == 4276803 >> ' + (result.toString() == '4276803'));
		
		result = this.stringToBigInt('ABCD');
		console.log('test 15 (stringToBigInt) >> ' + result.toString() + ' == 1094861636 >> ' + (result.toString() == '1094861636'));
	}
	
};