﻿function Shop($http) {

	this.http = $http;

	this.loadProducts = function() {
		return [{
				id: 1,
				name: 'Pralka',
				description: 'Super nowoczesna pralka, sama prasuje i zbiera skarpetki!',
				price: 1499.99
			},
			{
				id: 2,
				name: 'Zmywarka',
				description: 'Super nowoczesna zmywarka, zbiera naczynia i układa w szafie!',
				price: 1199.99
			}
		];
	}

};