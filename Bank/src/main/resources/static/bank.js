﻿function Bank($http, offline) {

	this.http = $http;
	this.OFFLINE = offline;

	this.get1 = function(callback) {
		if (this.OFFLINE) {
			callback.call('Powiedzmy, że się udało :)');
		}
		else {
			$http.post('/method1').then(
				function(result) {
					// success
					callback.call(result.data);
				},
				function(result) {
					// failure
				});
		}
	}
	
	this.get2 = function(callback) {
		if (this.OFFLINE) {
			callback.call('Powiedzmy, że się udało :)');
		}
		else {
			$http.post('/method2').then(
				function(result) {
					// success
					callback.call(result.data);
				},
				function(result) {
					// failure
				});
		}
	}

};