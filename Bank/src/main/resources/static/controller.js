﻿var app = angular.module("shopApp", ['jsbn.BigInteger']);

app.controller("shopCtrl", function($scope, $http, BigInteger) {

	$scope.offline = false;
	
	$scope.bank = new Bank($http, $scope.offline);
	
	$scope.myNumber = 123;
	
	$scope.obj1 = null;
	$scope.obj2 = null;
	
	$scope.example = 'brak logów';
	
	$scope.init = function() {
		
	}
	
	$scope.action1 = function(item) {
		console.log('Wywołuję akcję pierwszą');
		$scope.bank.get1({
			call: function(result) {
				console.log('Akcja pierwsza wywołała się!');
				$scope.obj1 = result;
			}
		});
	}
	
	$scope.action2 = function(item) {
		console.log('Wywołuję akcję drugą');
		$scope.bank.get1({
			call: function(result) {
				console.log('Akcja druga wywołała się!');
				$scope.obj2 = result;
			}
		});
	}
	
});