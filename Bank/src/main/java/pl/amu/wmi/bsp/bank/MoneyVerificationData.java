package pl.amu.wmi.bsp.bank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude()
public class MoneyVerificationData {
    @JsonProperty("S")
    private String[] S;
    @JsonProperty("L")
    private String[] L;
    @JsonProperty("B")
    private String[] B;
    @JsonProperty("R")
    private String[] R;
    @JsonProperty("T")
    private String[] T;
    @JsonProperty("C")
    private String[] C;
    @JsonProperty("Z")
    private String Z;


}
