package pl.amu.wmi.bsp.bank;

import com.codepoetics.protonpack.StreamUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jooq.lambda.tuple.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.amu.wmi.bsp.shop.SignedMoney;
import pl.amu.wmi.bsp.shop.VerifiedMoney;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "/money")
public class BankController {

    private String[] blindedMoneys;
    private int j;
    private SecureRandom random = new SecureRandom();
    private static final KeyPair keyPair;
    private static final Map<String, VerifiedMoney> usedMoney = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(BankController.class);

    static {
        KeyPairGenerator keyGen;
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
            //it's obvious that RSA there is!
        }
        keyGen.initialize(1024);
        keyPair = keyGen.generateKeyPair();
    }

    @RequestMapping(path = "/publicKey", method = RequestMethod.GET)
    public SimpleKey getPublicKey() {
        RSAPublicKey rsaKey = (RSAPublicKey) keyPair.getPublic();
        SimpleKey simpleKey = new SimpleKey(rsaKey.getModulus(), rsaKey.getPublicExponent());
        LOGGER.info("Returning public key pair: {}", simpleKey);
        return simpleKey;
    }

    @RequestMapping(path = "/init", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public int initMoney(@RequestBody String blindedMoneys) {
        //this.blindedMoneys = blindedMoneys;
        try {
            FileUtils.write(new File("/home/kacper/tmp/blindedMoneys.json"), blindedMoneys);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return j = Math.abs(random.nextInt()) % 100;
    }

    @RequestMapping(path = "/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String revealMoney(@RequestBody MoneyVerificationData[] moneyVerificationDatas) {
        List<Tuple2<InitialMoney, MoneyVerificationData>> tuples = StreamUtils
                .zip(Stream.of(blindedMoneys), Stream.of(moneyVerificationDatas), Tuple2::new)
                .map(this::unblindMoney)
                .collect(Collectors.toList());
        InitialMoney[] initialMoneys = tuples.stream().map(Tuple2::v1).toArray(size -> new InitialMoney[size]);
        verifyInitialMoneys(initialMoneys);
        verifyZippedMoneys(tuples);
        RSAPrivateKey aPrivate = (RSAPrivateKey) keyPair.getPrivate();
        return new BigInteger(blindedMoneys[j].getBytes()).modPow(aPrivate.getPrivateExponent(), aPrivate.getModulus()).toString();
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void useMoney(@RequestBody  VerifiedMoney verifiedMoney) {
        SignedMoney signedMoney = verifiedMoney.getSignedMoney();
        verifySignedMoney(signedMoney);
        InitialMoney initialMoney = signedMoney.asInitialMoney();
        verifyX(initialMoney);
    }

    private void verifyX(InitialMoney initialMoney) {
        if(usedMoney.get(initialMoney.getX())  != null){
            throw new IllegalStateException("Money used in previouse dupa dupa...");
        }
    }

    private void verifySignedMoney(SignedMoney signedMoney) {
        String M = signedMoney.getM();
        String S = signedMoney.getS();
        SimpleKey bankPublicKey = getPublicKey();
        boolean equals = Arrays.equals(new BigInteger(M.getBytes()).modPow(bankPublicKey.getExp(), bankPublicKey.getMod()).toByteArray(), S.getBytes());
        if(!equals) {
            throw new IllegalArgumentException();
        }
    }

    private void verifyZippedMoneys(List<Tuple2<InitialMoney, MoneyVerificationData>> tuples) {
        boolean allMatch = tuples.stream().allMatch(this::isZippedMoneyValid);
        if (!allMatch) {
            throw new GoToJailException();
        }
    }

    private boolean isZippedMoneyValid(Tuple2<InitialMoney, MoneyVerificationData> tuple) {
        InitialMoney initialMoney = tuple.v1();
        MoneyVerificationData moneyVerificationData = tuple.v2();
        return Arrays.equals(initialMoney.getS(), moneyVerificationData.getS()) &&
                Arrays.equals(initialMoney.getT(), moneyVerificationData.getT()) &&
                verifyHashes(initialMoney, moneyVerificationData);
    }

    private Tuple2<InitialMoney, MoneyVerificationData> unblindMoney(Tuple2<String, MoneyVerificationData> blindedMoneyTuple) {
        String blindedMoney = blindedMoneyTuple.v1();
        BigInteger Y = new BigInteger(blindedMoney.getBytes());
        MoneyVerificationData moneyVerificationData = blindedMoneyTuple.v2();
        BigInteger Z = new BigInteger(moneyVerificationData.getZ());
        SimpleKey publicKey = getPublicKey();
        BigInteger E = publicKey.getExp();
        BigInteger N = publicKey.getMod();
        String rawJson = new String(Y.mod(N).multiply(Z.modPow(E.negate(), N)).mod(N).toByteArray());
        try {
            InitialMoney initialMoney = new ObjectMapper().readValue(rawJson, InitialMoney.class);
            return new Tuple2<>(initialMoney, moneyVerificationData);
        } catch (IOException e) {
            throw new RuntimeException("Cannot parse rawJson : " + rawJson, e);
        }
    }

    private boolean verifyHashes(InitialMoney initialMoney, MoneyVerificationData moneyVerificationData) {
        boolean result = true;
        for (int i = 0; i < 100; i++) {
            String[] S = moneyVerificationData.getS();
            String[] B = moneyVerificationData.getB();
            String[] L = moneyVerificationData.getL();
            result &= StringUtils.equals(Hashing.sha256().hashUnencodedChars(S[i].concat(B[i]).concat(L[i])).toString(), initialMoney.getU()[i]);
            result &= StringUtils.equals(S[i], initialMoney.getS()[i]);

            String[] T = moneyVerificationData.getT();
            String[] C = moneyVerificationData.getC();
            String[] R = moneyVerificationData.getR();
            result &= StringUtils.equals(Hashing.sha256().hashUnencodedChars(T[i].concat(C[i]).concat(R[i])).toString(), initialMoney.getW()[i]);
            result &= StringUtils.equals(T[i], initialMoney.getT()[i]);

            if(!result) {
                break;
            }
        }
        return result;
    }

    private void verifyInitialMoneys(InitialMoney[] initialMoneys) {
        if (ArrayUtils.getLength(initialMoneys) != 100
                && !areAllMoneyAmountTheSame(initialMoneys)
                && !areAllIdsUnique(initialMoneys)
                && !Arrays.stream(initialMoneys).allMatch(this::areSubarraysValid)) {
            throw new WrongInitialMoneyDataException();
        }
    }

    private boolean areSubarraysValid(InitialMoney initialMoney) {
        return isLength100(initialMoney.getS())
                && isLength100(initialMoney.getT())
                && isLength100(initialMoney.getU())
                && isLength100(initialMoney.getW());
    }

    private <T> boolean isLength100(T[] ts) {
        return ArrayUtils.getLength(ts) == 100;
    }

    private boolean areAllMoneyAmountTheSame(InitialMoney[] initialMoneys) {
        return Arrays.stream(initialMoneys).map(initialMoney -> initialMoney.getY()).allMatch(Predicate.isEqual(initialMoneys[0]));
    }

    private boolean areAllIdsUnique(InitialMoney[] initialMoneys) {
        return Arrays.stream(initialMoneys).map(InitialMoney::getX).collect(Collectors.toSet()).size() == initialMoneys.length;
    }
}
