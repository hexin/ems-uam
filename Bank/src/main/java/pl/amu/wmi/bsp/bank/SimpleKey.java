package pl.amu.wmi.bsp.bank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigInteger;

public class SimpleKey {

    private BigInteger mod;
    private BigInteger exp;

    public SimpleKey(BigInteger mod, BigInteger exp) {
        this.mod = mod;
        this.exp = exp;
    }

    @JsonProperty("N")
    @JsonSerialize(using = BigIntegerSerializer.class)
    public BigInteger getMod() {
        return mod;
    }

    @JsonProperty("E")
    @JsonSerialize(using = BigIntegerSerializer.class)
    public BigInteger getExp() {
        return exp;
    }

    @Override
    public String toString() {
        return "SimpleKey{" +
                "mod=" + mod +
                ", exp=" + exp +
                '}';
    }
}
