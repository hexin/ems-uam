package pl.amu.wmi.bsp.shop;

import com.google.common.hash.Hashing;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pl.amu.wmi.bsp.bank.InitialMoney;
import pl.amu.wmi.bsp.bank.SimpleKey;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.Arrays;

@RestController
@RequestMapping("/shop")
public class ShopController {

    @Value("bank.url")
    private String bankUrl;
    private SimpleKey bankPublicKey;
    private SignedMoney signedMoney;
    private InitialMoney initialMoney;
    private char[] chosenBitsArray;

    @PostConstruct
    public void initializeBanksPublicKey() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<SimpleKey> simpleKeyResponseEntity = restTemplate.getForEntity(bankUrl + "/money/publicKey", SimpleKey.class);
        bankPublicKey = simpleKeyResponseEntity.getBody();
    }

    @RequestMapping(value = "/init", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initPurchase(@RequestBody SignedMoney signedMoney) {
        verifySignedMoney(signedMoney);
        this.signedMoney = signedMoney;
        this.initialMoney = signedMoney.asInitialMoney();
        String chosenBits = RandomStringUtils.random(100, '0', '1');
        this.chosenBitsArray = chosenBits.toCharArray();
        return chosenBits;
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void finishPurchase(@RequestBody ShopMoneyVerificationData[] verificationDatas) {
        verifyShopMoney(verificationDatas);
        sendMoneyToBank(verificationDatas);
    }

    private void verifyShopMoney(ShopMoneyVerificationData[] verificationDatas) {
        for (int i = 0; i < verificationDatas.length; i++) {
            if (chosenBitsArray[i] == '0')
                verifyLiabilities(verificationDatas[i], initialMoney.getS()[i], initialMoney.getU()[i]);
            else
                verifyLiabilities(verificationDatas[i], initialMoney.getT()[i], initialMoney.getW()[i]);
        }
    }

    private void verifyLiabilities(ShopMoneyVerificationData verificationData, String ST, String UW) {
        String vCB = verificationData.getCB();
        String vRL = verificationData.getRL();
        String vTS = verificationData.getTS();
        if(!verificationData.getTS().equals(ST) || !StringUtils.equals(Hashing.sha256().hashUnencodedChars(vTS.concat(vCB).concat(vRL)).toString(), UW)) {
            throw new IllegalArgumentException();
        }

    }

    private void verifySignedMoney(SignedMoney signedMoney) {
        String M = signedMoney.getM();
        String S = signedMoney.getS();
        boolean equals = Arrays.equals(new BigInteger(M.getBytes()).modPow(bankPublicKey.getExp(), bankPublicKey.getMod()).toByteArray(), S.getBytes());
        if(!equals) {
            throw new IllegalArgumentException();
        }
    }

    private void sendMoneyToBank(ShopMoneyVerificationData[] verificationDatas) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> objectResponseEntity = restTemplate.postForEntity(bankUrl + "/money/use", new VerifiedMoney(signedMoney, verificationDatas), Object.class);
    }
}
