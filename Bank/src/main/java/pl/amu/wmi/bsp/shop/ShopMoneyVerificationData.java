package pl.amu.wmi.bsp.shop;

/**
 * Created by kacper on 22.01.16.
 */
public class ShopMoneyVerificationData {

    private String TS;
    private String CB;
    private String RL;

    public ShopMoneyVerificationData(String TS, String CB, String RL) {
        this.TS = TS;
        this.CB = CB;
        this.RL = RL;
    }

    public String getTS() {
        return TS;
    }

    public String getCB() {
        return CB;
    }

    public String getRL() {
        return RL;
    }
}
