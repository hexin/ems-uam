package pl.amu.wmi.bsp.shop;

import com.fasterxml.jackson.databind.ObjectMapper;
import pl.amu.wmi.bsp.bank.InitialMoney;

import java.io.IOException;

public class SignedMoney {
    private String M;
    private String S;

    public SignedMoney(String M, String S) {
        this.M = M;
        this.S = S;
    }

    public String getM() {
        return M;
    }

    public String getS() {
        return S;
    }

    public InitialMoney asInitialMoney() {
        try {
            return new ObjectMapper().readValue(M, InitialMoney.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
