package pl.amu.wmi.bsp.bank;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Initial Money Data are very bad")  // 404
public class GoToJailException extends RuntimeException {
}
