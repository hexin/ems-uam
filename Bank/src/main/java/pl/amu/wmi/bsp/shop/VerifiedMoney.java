package pl.amu.wmi.bsp.shop;

/**
 * Created by kacper on 23.01.16.
 */
public class VerifiedMoney {
    private SignedMoney signedMoney;
    private ShopMoneyVerificationData[] verificationDatas;

    public VerifiedMoney(SignedMoney signedMoney, ShopMoneyVerificationData[] verificationDatas) {
        this.signedMoney = signedMoney;
        this.verificationDatas = verificationDatas;
    }

    public SignedMoney getSignedMoney() {
        return signedMoney;
    }

    public void setSignedMoney(SignedMoney signedMoney) {
        this.signedMoney = signedMoney;
    }

    public ShopMoneyVerificationData[] getVerificationDatas() {
        return verificationDatas;
    }

    public void setVerificationDatas(ShopMoneyVerificationData[] verificationDatas) {
        this.verificationDatas = verificationDatas;
    }
}
