package pl.amu.wmi.bsp.bank;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.math.BigDecimal;

@Data
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude()
public class InitialMoney {
    @JsonProperty("Y")
    private BigDecimal Y;
    @JsonProperty("X")
    private String X;
    @JsonProperty("S")
    private String[] S;
    @JsonProperty("T")
    private String[] T;
    @JsonProperty("U")
    private String[] U;
    @JsonProperty("W")
    private String[] W;
}
